package pl.edu.ajp.student.kamil.ajplayer.UI.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;

import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.AllStationItem;
import pl.edu.ajp.student.kamil.ajplayer.R;

public class StationActivity extends AppCompatActivity {

    public static final String ALL_STATION_ITEM_KEY = "AllStationItem";

    private TextView stationName;
    private TextView stationUrl;
    private AllStationItem allStationItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_station_item);

        allStationItem = new Gson()
                .fromJson(getIntent()
                .getSerializableExtra(ALL_STATION_ITEM_KEY)
                .toString(), AllStationItem.class);

        stationName = findViewById(R.id.txt_nameStation);
        stationUrl = findViewById(R.id.txt_urlStation);

        stationName.setText(allStationItem.name);
//        stationUrl.setText(allStationItem.); // Wymaga poprawienia modelu
    }
}
