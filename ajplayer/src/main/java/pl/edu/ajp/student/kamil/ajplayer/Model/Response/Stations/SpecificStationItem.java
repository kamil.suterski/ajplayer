package pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations;

import java.util.List;

import pl.edu.ajp.student.kamil.ajplayer.Model.Domains.Category;
import pl.edu.ajp.student.kamil.ajplayer.Model.Domains.Stream;

public class SpecificStationItem extends AllStationItem {

    public String total_listeners;
    public List<Stream> streams;
    public List<Category> categories;
}
