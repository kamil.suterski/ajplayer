package pl.edu.ajp.student.kamil.ajplayer.Api;


import java.util.List;
import java.util.Map;

import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Categories.AllCategoryItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Categories.CategoryStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Categories.ChildCategoryItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Categories.PrimariesCategoryItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Countries.AllContinentItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Countries.AllCountryItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Countries.CountriesInContinentsItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Countries.StationFromCountryItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Search.SearchItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Songs.RecentPlayedSongItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.AllStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.PopularStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.RecentAddedStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.SimilarStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.SongHistoryForAStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.SpecificStationItem;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiInterface {

//All Stations
//GET http://api.dirble.com/v2/stations?token={your token}



////  Pagination

// Get All Stations
// GET http://api.dirble.com/v2/stations?page=1&per_page=10&token={your token}
// Query     Default Value   Description
// page	    0	            show which per_page stations to show
// per_page	20	            How many stations per page to show
// offset	0

    @GET("v2/stations")
    Call<List<AllStationItem>> getAllStations(
            @Query("token") String token
    );


    @GET("v2/stations")
    Call<List<AllStationItem>> getAllStationsWithPagiantionParams(
            @Query("token") String token,
            @QueryMap Map<String, String> map
    );



//    @GET("v2/stations")
//    Call<List<AllStationItem>> getAllStations(
//            @Query("page") int page,
//            @Query("per_page") int per_page,
//            @Query("offset") int offset,
//            @Query("token") String token);

//Get Recent added Stations
//GET http://api.dirble.com/v2/stations/recent={your token}
//Query     Default Value   Description
//page	    0	            show which per_page stations to show
//per_page	20	            How many stations per page to show
//offset	0

    @GET("v2/stations")
    Call<List<RecentAddedStationItem>> getRecentAddedStations(
            @Query("token") String token,
            @QueryMap Map<String, String> map
    );

//Get Popular Stations
//GET http://api.dirble.com/v2/stations/popular={your token}
//Parameter	    Default	    Description
//page	        0	        show which per_page stations to show
//per_page	    20	        How many stations per page to show
//offset  	    0

    @GET("v2/stations/popular")
    Call<List<PopularStationItem>> getPopularStation(
            @Query("token") String token,
            @QueryMap Map<String, String> map
    );

//Get a Specific Station
//GET http://api.dirble.com/v2/station/<ID>={your token}
//ID	The ID of the station to retrieve

    @GET("v2/station/{id}")
    Call<List<SpecificStationItem>> getSpecificStation(
            @Path("id") int id,
            @Query("token") String token
    );

//Get Similar Stations
//GET http://api.dirble.com/v2/station/<ID>/similar={your token}
//ID	The ID of the station to get similar stations from

    @GET("v2/station/{id}/similar")
    Call<List<SimilarStationItem>> getSimilarStation(
            @Path("id") int id,
            @Query("token") String token
    );

//Get Songhistory for a Station
//GET http://api.dirble.com/v2/station/<ID>/song_history={your token}
//ID	The ID of the station to get song_history from

    @GET("v2/station/{id}/song_history")
    Call<List<SongHistoryForAStationItem>> getSongHistoryForAStation(
            @Path("id") String id,
            @Query("token") String token
    );

//-----------------------------------------------------------

// Songs
//GET http://api.dirble.com/v2/songs
// None parameters
    @GET("v2/songs")
    Call<List<RecentPlayedSongItem>> getRecentPlayedSongs(
            @Query("token") String token
    );

// Get Primaries Categories
//GET http://api.dirble.com/v2/categories/primary={your token}
// None parameters

    @GET("v2/categories/primary")
    Call<List<AllCategoryItem>> getAllCategories(
            @Query("token") String token
    );

// Get Child Categories
//GET http://api.dirble.com/v2/category/<ID>/childs
//Parameter     Default	    Description
//id		    (Required), id of primary category to show child categories of
//page	        0	        show which per_page stations to show
//per_page	    20	        How many stations per page to show
//offset	    0

    @GET("v2/category/{id}/childs")
    Call<List<ChildCategoryItem>> getChildCategories(
            @Path("id") String id,
            @Query("token") String token,
            @QueryMap Map<String, String> map
    );

// Get Categories as tree
// GET http://api.dirble.com/v2/categories/primary={your token}
// None parameters

    @GET("v2/categories/primary")
    Call<List<PrimariesCategoryItem>> getPrimariesCategories(
            @Query("token") String token
    );

// Get Category Stations
// GET http://api.dirble.com/v2/category/<ID>/stations
//Parameter	    Default	    Description
//id		    (Required), id of category to get stations of
//page    	    0	        show which per_page stations to show
//per_page	    20	        How many stations per page to show
//offset	    0

    @GET("v2/category/{id}/stations")
    Call<List<CategoryStationItem>> getCategoryStations(
            @Query("token") String token,
            @QueryMap Map<String, String> map
    );

//-----------------------------------------------------------

//Search
//POST http://api.dirble.com/v2/search={your token}
//Header:
// "Content-Type: application/json"
//Parameteres:
//Parameter     Default	    Description
//query		    (Required), the search query to search on
//page	        0   	    show which 10 stations to show
//category	    null	    id for category to filter by
//country	    null	    alpha2 of country to filter by

    @GET("v2/search")
    Call<List<SearchItem>> getSearch(
            @Query("token") String token,
            @QueryMap Map<String, String> map
    );

//-----------------------------------------------------------

//Countries
//GET http://api.dirble.com/v2/countries?token={your token}
// No parameters

    @GET("v2/countries")
    Call<List<AllCountryItem>> getAllCountries(
            @Query("token") String token
    );

//Get All Continents
// GET http://api.dirble.com/v2/continents={your token}
// No parameters

    @GET("v2/continents")
    Call<List<AllContinentItem>> getAllContinent(
            @Query("token") String token
    );

// Get Countries in Continents
//GET http://api.dirble.com/v2/continents/<continent>/countries
//Parameter	    Default	    Description
//continent		(Required), the continent id to get countries from
//page	        0	        show which per_page stations to show
//per_page	    20	        How many stations per page to show
//offset	    0

    @GET("v2/continents/{continent}/countries")
    Call<List<CountriesInContinentsItem>> getCountriesInContinents(
            @Path("continent") String continent,
            @Query("token") String token,
            @QueryMap Map<String, String> map
    );

//Get Station from Country
//GET http://api.dirble.com/v2/countries/<code>/stations
//Parameter	    Default	    Description
//code		    (Required), the country code for the country to get stations from
//page	        0	        show which per_page stations to show
//per_page	    20	        How many stations per page to show
//offset	    0

    @GET("v2/countries/<code>/stations")
    Call<List<StationFromCountryItem>> getStationFromCountryItem(
            @Path("code") String code,
            @Query("token") String token,
            @QueryMap Map<String, String> map
    );

}
