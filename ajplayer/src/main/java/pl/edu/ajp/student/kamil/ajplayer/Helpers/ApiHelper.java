package pl.edu.ajp.student.kamil.ajplayer.Helpers;

import java.util.HashMap;
import java.util.Map;

public class ApiHelper {

    public static Map<String, String> convertArgsToMap(String[] args) {
        Map<String, String> map = new HashMap<>();

        if (args.length < 4 && checkStringIsNumber(args) ) {
            for(int i=0; i < args.length; i++) {
                switch (i) {
                    case 0:
                        map.put("page",args[i]);
                        break;
                    case 1:
                        map.put("per_page", args[i]);
                        break;
                    case 2:
                        map.put("offset", args[i]);
                        break;
                }
            }
        }
        return map;
    }

    public static boolean checkStringIsNumber(String[] args) {
        boolean success = false;

        for ( String arg : args) {
            try {
                Integer.parseInt(arg);
                success = true;
            } catch (Exception e){
                success = false;
            }
        }
        return success;
    }
}
