package pl.edu.ajp.student.kamil.ajplayer.UI.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import pl.edu.ajp.student.kamil.ajplayer.AJPlayerApp;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.AllStationItem;
import pl.edu.ajp.student.kamil.ajplayer.R;
import pl.edu.ajp.student.kamil.ajplayer.UI.Activities.StationActivity;
import pl.edu.ajp.student.kamil.ajplayer.UI.Adapters.StationsListFragmentAdapter;

public class StationListFragment extends Fragment implements StationsListFragmentAdapter.AllStationItemClickedListener {

    public static final String ALL_STATION_ITEM_KEY = "AllStationItem";
    private RecyclerView recyclerView;
    private AJPlayerApp ajPlayerApp;

    public StationListFragment() {
        // Required empty public constructor
    }

    public static StationListFragment newInstance() {

        StationListFragment fragment = new StationListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ajPlayerApp = (AJPlayerApp) getActivity().getApplication();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_all_station, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) getView().findViewById(R.id.recyclerViewAllStation);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        StationsListFragmentAdapter adapter = new StationsListFragmentAdapter(
                ajPlayerApp.getRepository().getAllStationItemList());
        adapter.setAllStationItemClickedListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void allStationItemClicked(AllStationItem allStationItem) {

        Intent intent = new Intent(getActivity(), StationActivity.class);
        intent.putExtra(ALL_STATION_ITEM_KEY, new Gson().toJson(allStationItem));
        startActivity(intent);
    }
}
