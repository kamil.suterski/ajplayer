package pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations;

import java.util.Date;

import pl.edu.ajp.student.kamil.ajplayer.Model.Subclass.ImageData;

public class AllStationItem {
    public Integer id;
    public String name;
    public String description;
    public String country;
    public String website;
    public ImageData image;

    public Date created_at;
    public Date updated_at;

    public String slug;
}
