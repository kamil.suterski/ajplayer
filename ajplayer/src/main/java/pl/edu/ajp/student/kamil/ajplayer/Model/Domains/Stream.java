package pl.edu.ajp.student.kamil.ajplayer.Model.Domains;

public class Stream {

    public String stream;
    public int bitrate;
    public String content_type;
    public int listeners;
    public int status;
}
