package pl.edu.ajp.student.kamil.ajplayer.Model.Response.Countries;

import java.util.Date;
import java.util.List;

import pl.edu.ajp.student.kamil.ajplayer.Model.Domains.Category;
import pl.edu.ajp.student.kamil.ajplayer.Model.Domains.Stream;
import pl.edu.ajp.student.kamil.ajplayer.Model.Subclass.ImageData;

public class StationFromCountryItem {

    public int id;
    public String name;
    public int accepted;
    public String country;
    public String description;
    public int total_listeners;
    public ImageData image;
    public String slug;
    public String website;
    public Date created_at;
    public Date updated_at;
    public List<Stream> streams;
    public List<Category> categories;

}
