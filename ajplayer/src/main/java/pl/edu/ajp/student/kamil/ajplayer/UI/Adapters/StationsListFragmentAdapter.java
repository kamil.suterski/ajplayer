package pl.edu.ajp.student.kamil.ajplayer.UI.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.AllStationItem;
import pl.edu.ajp.student.kamil.ajplayer.R;

public class StationsListFragmentAdapter extends RecyclerView.Adapter<StationsListFragmentAdapter.AllStationsViewHolder> {

    private List<AllStationItem> allStationItemList;

    private AllStationItemClickedListener allStationItemClickedListener;

    public StationsListFragmentAdapter(List<AllStationItem> allStationItemList) {
        this.allStationItemList = allStationItemList;
    }

    public void setAllStationItemClickedListener(AllStationItemClickedListener allStationItemClickedListener) {
        this.allStationItemClickedListener = allStationItemClickedListener;
    }

    @NonNull
    @Override
    public AllStationsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.all_station_item_layout, parent, false);
        return new AllStationsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllStationsViewHolder holder, int position) {
        AllStationItem allStationItem = allStationItemList.get(position);
        holder.setStationData(allStationItem);
    }

    @Override
    public int getItemCount() {
        return allStationItemList.size();
    }

    private void allStationItemClicked(AllStationItem allStationItem) {
        if(allStationItemClickedListener != null ) {
            allStationItemClickedListener.allStationItemClicked(allStationItem);
        }
    }

    class AllStationsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private AllStationItem allStationItem;

        private TextView nameAllStationItem;
        private String urlAllStationItem;

        public AllStationsViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            nameAllStationItem = (TextView) itemView.findViewById(R.id.nameAllStationItem);
        }

        public void setStationData(AllStationItem allStationItem) {
            this.allStationItem = allStationItem;

            nameAllStationItem.setText(allStationItem.name);
        }

        @Override
        public void onClick(View v) {
            allStationItemClicked(allStationItem);
        }
    }

    public interface AllStationItemClickedListener {
        void allStationItemClicked(AllStationItem allStationItem);
    }
}

