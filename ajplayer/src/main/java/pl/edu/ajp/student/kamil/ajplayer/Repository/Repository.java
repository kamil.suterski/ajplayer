package pl.edu.ajp.student.kamil.ajplayer.Repository;

import java.util.List;

import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Categories.AllCategoryItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Categories.CategoryAsTreeItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Categories.CategoryStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Categories.PrimariesCategoryItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Countries.AllContinentItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Countries.AllCountryItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Countries.CountriesInContinentsItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Countries.StationFromCountryItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Search.SearchItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Songs.RecentPlayedSongItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.AllStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.PopularStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.RecentAddedStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.SimilarStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.SongHistoryForAStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.SongHistoryStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.SpecificStationItem;

public class Repository {

    // Categories
    private List<AllCategoryItem> allCategoryItemList;
    private List<CategoryAsTreeItem> categoryAsTreeItemList;
    private List<CategoryStationItem> categoryStationItemList;
    private List<PrimariesCategoryItem> primariesCategoryItemList;

    //Countries
    private List<AllContinentItem> allContinentItemList;
    private List<AllCountryItem> allCountryItemList;
    private List<CountriesInContinentsItem> countriesInContinentsItemList;
    private List<StationFromCountryItem> stationFromCountryItemList;

    // Search
    private List<SearchItem> searchItemList;

    // Songs
    private List<RecentPlayedSongItem> recentPlayedSongItemList;

    // Stations
    public List<AllStationItem> allStationItemList;
    private List<PopularStationItem> popularStationItemList;
    private List<RecentAddedStationItem> recentAddedStationItemList;
    private List<SimilarStationItem> similarStationItemList;
    private List<SongHistoryForAStationItem> songHistoryForAStationItemList;
    private List<SongHistoryStationItem> songHistoryStationItemList;
    private List<SpecificStationItem> specificStationItemList;

    public Repository() {
    }

    public List<AllCategoryItem> getAllCategoryItemList() {
        return allCategoryItemList;
    }

    public void setAllCategoryItemList(List<AllCategoryItem> allCategoryItemList) {
        this.allCategoryItemList = allCategoryItemList;
    }

    public List<CategoryAsTreeItem> getCategoryAsTreeItemList() {
        return categoryAsTreeItemList;
    }

    public void setCategoryAsTreeItemList(List<CategoryAsTreeItem> categoryAsTreeItemList) {
        this.categoryAsTreeItemList = categoryAsTreeItemList;
    }

    public List<CategoryStationItem> getCategoryStationItemList() {
        return categoryStationItemList;
    }

    public void setCategoryStationItemList(List<CategoryStationItem> categoryStationItemList) {
        this.categoryStationItemList = categoryStationItemList;
    }

    public List<PrimariesCategoryItem> getPrimariesCategoryItemList() {
        return primariesCategoryItemList;
    }

    public void setPrimariesCategoryItemList(List<PrimariesCategoryItem> primariesCategoryItemList) {
        this.primariesCategoryItemList = primariesCategoryItemList;
    }

    public List<AllContinentItem> getAllContinentItemList() {
        return allContinentItemList;
    }

    public void setAllContinentItemList(List<AllContinentItem> allContinentItemList) {
        this.allContinentItemList = allContinentItemList;
    }

    public List<AllCountryItem> getAllCountryItemList() {
        return allCountryItemList;
    }

    public void setAllCountryItemList(List<AllCountryItem> allCountryItemList) {
        this.allCountryItemList = allCountryItemList;
    }

    public List<CountriesInContinentsItem> getCountriesInContinentsItemList() {
        return countriesInContinentsItemList;
    }

    public void setCountriesInContinentsItemList(List<CountriesInContinentsItem> countriesInContinentsItemList) {
        this.countriesInContinentsItemList = countriesInContinentsItemList;
    }

    public List<StationFromCountryItem> getStationFromCountryItemList() {
        return stationFromCountryItemList;
    }

    public void setStationFromCountryItemList(List<StationFromCountryItem> stationFromCountryItemList) {
        this.stationFromCountryItemList = stationFromCountryItemList;
    }

    public List<SearchItem> getSearchItemList() {
        return searchItemList;
    }

    public void setSearchItemList(List<SearchItem> searchItemList) {
        this.searchItemList = searchItemList;
    }

    public List<RecentPlayedSongItem> getRecentPlayedSongItemList() {
        return recentPlayedSongItemList;
    }

    public void setRecentPlayedSongItemList(List<RecentPlayedSongItem> recentPlayedSongItemList) {
        this.recentPlayedSongItemList = recentPlayedSongItemList;
    }

    public List<AllStationItem> getAllStationItemList() {
        return allStationItemList;
    }

    public void setAllStationItemList(List<AllStationItem> allStationItemList) {
        this.allStationItemList = allStationItemList;
    }

    public List<PopularStationItem> getPopularStationItemList() {
        return popularStationItemList;
    }

    public void setPopularStationItemList(List<PopularStationItem> popularStationItemList) {
        this.popularStationItemList = popularStationItemList;
    }

    public List<RecentAddedStationItem> getRecentAddedStationItemList() {
        return recentAddedStationItemList;
    }

    public void setRecentAddedStationItemList(List<RecentAddedStationItem> recentAddedStationItemList) {
        this.recentAddedStationItemList = recentAddedStationItemList;
    }

    public List<SimilarStationItem> getSimilarStationItemList() {
        return similarStationItemList;
    }

    public void setSimilarStationItemList(List<SimilarStationItem> similarStationItemList) {
        this.similarStationItemList = similarStationItemList;
    }

    public List<SongHistoryForAStationItem> getSongHistoryForAStationItemList() {
        return songHistoryForAStationItemList;
    }

    public void setSongHistoryForAStationItemList(List<SongHistoryForAStationItem> songHistoryForAStationItemList) {
        this.songHistoryForAStationItemList = songHistoryForAStationItemList;
    }

    public List<SongHistoryStationItem> getSongHistoryStationItemList() {
        return songHistoryStationItemList;
    }

    public void setSongHistoryStationItemList(List<SongHistoryStationItem> songHistoryStationItemList) {
        this.songHistoryStationItemList = songHistoryStationItemList;
    }

    public List<SpecificStationItem> getSpecificStationItemList() {
        return specificStationItemList;
    }

    public void setSpecificStationItemList(List<SpecificStationItem> specificStationItemList) {
        this.specificStationItemList = specificStationItemList;
    }
}
