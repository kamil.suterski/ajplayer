package pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations;

import java.util.List;

import pl.edu.ajp.student.kamil.ajplayer.Model.Domains.Category;
import pl.edu.ajp.student.kamil.ajplayer.Model.Domains.Stream;

public class RecentAddedStationItem extends AllStationItem {

    public String twitter;
    public String facebook;
    public List<Category> categories;
    public List<Stream> streams;
}
