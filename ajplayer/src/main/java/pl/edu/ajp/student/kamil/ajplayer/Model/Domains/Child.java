package pl.edu.ajp.student.kamil.ajplayer.Model.Domains;

import java.util.Date;
import java.util.List;

public class Child {

    public int id;
    public String title;
    public String description;
    public String urlid;
    public Date created_at;
    public Date updated_at;
    public String slug;
    public int ancestry;
    public int position;
    public List<Child> children;

}
