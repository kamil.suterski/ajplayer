package pl.edu.ajp.student.kamil.ajplayer;

import android.app.Application;

import pl.edu.ajp.student.kamil.ajplayer.Api.ApiManager;
import pl.edu.ajp.student.kamil.ajplayer.Repository.Repository;

public class AJPlayerApp extends Application {

    private ApiManager apiManager;
    private Repository repository;

    @Override
    public void onCreate() {
        super.onCreate();

        repository = new Repository();
        apiManager = new ApiManager(repository);
        getApiManager().getAllStationsDefault(); // Dodane jedynie do testów

    }

    public ApiManager getApiManager() {
        return apiManager;
    }

    public Repository getRepository() {
        return repository;
    }
}
