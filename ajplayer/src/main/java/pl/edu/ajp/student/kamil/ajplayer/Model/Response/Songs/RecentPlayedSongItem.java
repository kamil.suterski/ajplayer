package pl.edu.ajp.student.kamil.ajplayer.Model.Response.Songs;

import pl.edu.ajp.student.kamil.ajplayer.Model.Subclass.InfoData;

public class RecentPlayedSongItem {

    public int id;
    public String name;
    public String title;
    public int week;
    public int year;

    public InfoData info;

}
