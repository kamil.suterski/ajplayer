package pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations;

import java.util.List;

import pl.edu.ajp.student.kamil.ajplayer.Model.Domains.Category;
import pl.edu.ajp.student.kamil.ajplayer.Model.Domains.Stream;

public class PopularStationItem extends AllStationItem {

    public String twitter;
    public String facebook;
    public int total_listeners;
    public List<Stream> streams;
    public List<Category> categories;

}
