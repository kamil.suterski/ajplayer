package pl.edu.ajp.student.kamil.ajplayer.Api;

import android.util.Log;
import java.util.List;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations.AllStationItem;
import pl.edu.ajp.student.kamil.ajplayer.Repository.Repository;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static pl.edu.ajp.student.kamil.ajplayer.Helpers.ApiHelper.convertArgsToMap;

public class ApiManager {

    private static final String API_KEY = "42975874b1ed74966f6c60d441";
    public static final String API_BASE_URL = "http://api.dirble.com/";

    private Retrofit retrofit;
    private Repository repository;
    private ApiInterface client;

    public ApiManager() {
    }

    public ApiManager(Repository repository) {
        initRetrofit();
        this.repository = repository;
    }

    public void initRetrofit() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder().addNetworkInterceptor(loggingInterceptor).build();

        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(API_BASE_URL);
        builder.addConverterFactory(GsonConverterFactory.create());
        builder.client(okHttpClient);
        retrofit = builder.build();

        client = retrofit.create(ApiInterface.class);
    }

    public void getAllStationsDefault() {

        Call<List<AllStationItem>> call = client.getAllStations(API_KEY);
        call.enqueue(new Callback<List<AllStationItem>>() {
            @Override
            public void onResponse(Call<List<AllStationItem>> call, Response<List<AllStationItem>> response) {
                repository.setAllStationItemList(response.body());
            }

            @Override
            public void onFailure(Call<List<AllStationItem>> call, Throwable t) {
                Log.d("CallFailure: ", t.toString());
            }
        });
    }

    // Get All Stations
    // GET http://api.dirble.com/v2/stations?page=1&per_page=10&token={your token}
    // Query     Default Value   Description
    // page	    0	            show which per_page stations to show
    // per_page	20	            How many stations per page to show
    // offset	0
    //
    //    @GET("v2/stations")
    //    Call<List<AllStationItem>> getAllStationsWithPagiantionParams(
    //            @Query("token") String token,
    //            @QueryMap Map<String, String> map
    //    );

    public void getAllStationsWithPagiantionParams(String token, String... args) {

        Call<List<AllStationItem>> call = client.getAllStationsWithPagiantionParams(API_KEY, convertArgsToMap(args));
        call.enqueue(new Callback<List<AllStationItem>>() {
            @Override
            public void onResponse(Call<List<AllStationItem>> call, Response<List<AllStationItem>> response) {
                repository.setAllStationItemList(response.body());
            }

            @Override
            public void onFailure(Call<List<AllStationItem>> call, Throwable t) {
                Log.d("CallFailure: ", t.toString());
            }
        });
    }
}







