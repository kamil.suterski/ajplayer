package pl.edu.ajp.student.kamil.ajplayer.Model.Domains;

public class Category {

    public int id;
    public String title;
    public String description;
    public String slug;
    public int ancestry;

}