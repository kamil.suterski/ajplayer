package pl.edu.ajp.student.kamil.ajplayer.Model.Response.Categories;

import java.util.Date;
import java.util.List;

import pl.edu.ajp.student.kamil.ajplayer.Model.Domains.Child;

public class CategoryAsTreeItem {

    public int id;
    public String title;
    public String description;
    public String urlid;
    public Date created_at;
    public Date updated_at;
    public String slug;
    public int ancestry;
    public String position;
    public List<Child> children;
}
