package pl.edu.ajp.student.kamil.ajplayer.Model.Response.Stations;

import java.util.Date;

public class SongHistoryStationItem {

    public String id; // tu jest inny format id
    public String name;
    public String title;
    public int week;
    public int year;
    public int station_id;
    public String info;
    public Date date;
}
